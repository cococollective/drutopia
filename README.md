The Drutopia distribution will be the flagship libre software product of the [Drutopia](http://drutopia.org/) initiative.

# Contributing

We'd love for you to get involved! First review our [Contributing Guide](https://gitlab.com/drutopia/drutopia/blob/8.x-1.x/CONTRIBUTING.md)

Then take note of the [technical guide](https://gitlab.com/drutopia/drutopia/wikis/technical-guide) and [criteria for extensions](https://gitlab.com/drutopia/drutopia/wikis/extension-criteria-and-candidates) when contributing.

# Documentation

You can find [meeting notes](https://gitlab.com/drutopia/drutopia-organization/wikis/meeting-notes) and other important documents in the [project's wiki](https://gitlab.com/drutopia/drutopia/wikis/home).