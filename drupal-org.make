api = 2
core = 8.x

projects[config_update][version] = 1.2
projects[config_update][subdir] = contrib
projects[features][version] = 3.2
projects[features][subdir] = contrib

projects[drutopia_article][version] = 1.x-dev
projects[drutopia_article][subdir] = contrib
projects[drutopia_blog][version] = 1.x-dev
projects[drutopia_blog][subdir] = contrib
projects[drutopia_comment][version] = 1.x-dev
projects[drutopia_comment][subdir] = contrib
projects[drutopia_core][version] = 1.x-dev
projects[drutopia_core][subdir] = contrib
projects[drutopia_page][version] = 1.x-dev
projects[drutopia_page][subdir] = contrib
projects[drutopia_site][version] = 1.x-dev
projects[drutopia_site][subdir] = contrib
projects[drutopia_user][version] = 1.x-dev
projects[drutopia_user][subdir] = contrib
