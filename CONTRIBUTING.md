Working draft of the CONTRIBUTING.MD

Hello and welcome to the Drutopia project! We’re excited to have you here. There is no shortage of ways we can collaborate with one another.

# First things first 

* Check out our [Drutopia manifesto](https://gitlab.com/drutopia/drutopia/wikis/manifesto) for the big picture inspiration behind this effort.
* We’ve got each others' backs here. The [Code of Conduct](https://gitlab.com/drutopia/drutopia/wikis/drutopia-code-of-conduct) outlines exactly how.
* Drutopia is both a project and a member-based [platform cooperative](https://en.wikipedia.org/wiki/Platform_cooperative). Here is how we make decisions - [Decision Making](https://gitlab.com/drutopia/drutopia/wikis/decision-making) Making]
* A bit more info on [Drupal](https://www.drupal.org/about), which is what Drutopia is built off of.


# Getting Plugged In

Reading this document right here is great and all, but we also encourage contributors to join the conversation through video calls and IRC conversations too.

## General Meetings
- We have general meetings every Third Tuesday from 12:00-1:00 EST.

You’ll need Zoom.

https://zoom.us/j/5148320453

Or join by phone:

    +1 646 568 7788 (US Toll) or +1 415 762 9988 (US Toll)
    Meeting ID: 514 832 0453

## IRC

For discussing ideas we use Internet Relay Chat (IRC), a free and open source chat system. You can join the IRC channel by [downloading Adium](https://adium.im/), an IRC chat client. If you have any questions or trouble getting set up contact us at info@drutopia.org and we'd be happy to help.

Once you've got your IRC client set up, join our Drutopia channel -

**Drutopia IRC Info**
Server: irc.freenode.net
Channel: #drutopia


# Types of Contributions

## Bug Reports/Testing

A bug report means something is broken, preventing normal/typical use of Drutopia.

## How to report a bug in Drutopia

1. Make sure the bug isn’t already resolved. Search for similar issues in the bug category
1. Make sure you can reproduce your problem on a clean installation of Drutopia.
  * If you’re also testing on your own Drutopia development instance, be sure to use the Master release branch, aka the Tests Passed channel.
  * If possible, submit a Pull Request with a failing test, or;
  * if you'd rather take matters into your own hands, try fix the bug yourself.
1. Make a report of everything you know about the bug so far by opening an issue in GitLab about it.

When the bug is fixed, you can usually expect to see an update posted on the reporting topic.

## Change Requests

### Code

We welcome coders of all backgrounds and skillsets! Drutopia is a great place to build and learn. We value collaboration and learning. 

To get started, 

1. Follow the [Drutopia Technical Guide](https://gitlab.com/drutopia/drutopia/wikis/technical-guide)
2. Install Drutopia
3. Join the conversation on IRC at #drutopia

Pull requests are the primary mechanism we use to change Drutopia.

Please make pull requests against the master branch.

### Design

Ultimately we hope to have a vibrant selection of designs for people building sites on Drutopia to choose from.  Drutopia designs will be implemented as Drupal themes, using Twig templates, but we hope that the targeted use cases of Drutopia profiles will make it much easier to produce a great-looking and great-working theme than the extremely expansive use cases a generic theme must support.

We also have need of designs to mock up better ways visitors and content editors can interact with existing features, and to prototype new features.

## Documentation

We document the project through README files and wiki pages on GitLab. We keep track of documentation needs by posting issues to the project repository. 

We try to keep in mind [these tips for beginner-friendly documentation](https://datamade.us/blog/better-living-through-documentation).  It's a high bar and there's lot's of work needed to get there.

See [the list of outstanding documentation issues](https://gitlab.com/drutopia/drutopia/issues?label_name%5B%5D=documentation) for places you can help!

## Feature Requests

If you've got a great idea, we want to hear about it!  Before making a suggestion, here are a few handy tips on what to consider:

* [Search to see if the feature has already been requested](https://gitlab.com/drutopia/drutopia/issues?label_name%5B%5D=suggestion).
* TODO write a section like this?  Check out What makes it into Drutopia core? - this explains the guidelines for what fits into the scope and aims of the project
* Remember, your suggestion can't get adopted if people don't understand it. Please provide as much detail and context as possible. Explain the use case and why it is likely to be common. The strongest vote in favor of any feature request is clear demand among potential users/members of Drutopia.
* When making the request, tag it with "suggestion".  TODO create issue template for this.


## Outreach

Drutopia's public presence includes our web site, our projects on Gitlab and Drupal.org, social media, and (gasp) in-person gatherings at conferences and meetups. Here are just some of the ways you can help Drutopia:

* [Sign up as a supporter](http://drutopia.org/)
* Retweet and share Drutopia content
* Join our social media team to tweet and post to Facebook on behalf of Drutopia
* Respond to questions and comments on social media
* Plan and/or host hack-a-thons, trainings and presentations
* Blog about Drutopia!

## Project Management

Do you have skills as a project manager or have interest in helping to plan, track and bring Drutopia features to completion?  Here are some of the things you can do to become involved in the project management aspect of the project:


* [Sign up as a supporter](https://drutopia.org) and find out more about the project
* Join our core project management team. Contact leslie@drutopia.org for more info on this.
* We can use help:
  * Creating user stories for a Drutopia initiative of interest to you
  * Breaking out the Drutopia initiatives into a set of tasks that can be worked on by the initiative team
  * Checking in on the progress of these tasks to ensure that overall Drutopia project schedules are being met
  * Reporting the status of an initiative to the initiative team


## Research

A major focus of Drutopia is building well designed tools that meet users' needs. Join the collaborative research effort to ensure that grassroots organization's voices are heard.

* Interview grassroots organizations (we have questions to get your started)
* Share our survey at [drutopia.org/survey](https://drutopia.org/survey) with people who would benefit from Drutopia
* Join our core research team to analyze our research data and make suggestions for the roadmap and enhancements to the platform. Contact clayton@drutopia.org for more info on this.